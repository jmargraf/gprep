from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules=[
        Extension("spline_predict",
                  ["spline_predict.pyx"],
                  libraries=["m"])
]

setup(
  name = "Spline_predict",
  cmdclass = {"build_ext": build_ext},
  ext_modules = ext_modules
)
